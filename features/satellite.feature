@satellite
Feature: logging a qso made throw a sat
  when logging a qso made throw a sat, special fields should be added

  Scenario: making a qso on es'hail 2
    Given I choose QO-100 option on satellite list
    When I log a QSO
    Then log record field SAT_NAME should be QO-100
    And log record field PROP_MODE should be SAT
    And log record field MODE should be SSB
    And log record field FREQ_RX should be 10 Ghz
    And log record field FREQ should be 2.4 Mhz
