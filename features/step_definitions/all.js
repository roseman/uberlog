const url = require('url')
var fs = require("fs");
const path = require('path')
const electron = require("electron")
const BrowserWindow = electron.remote.BrowserWindow;
const { Before, After, Given, When, Then } = require('cucumber')
const userDataPath = (electron.app || electron.remote.app).getPath('userData');

Before(function() {
  this.callsign = "d3mo";
  // create fake config file
  this.config = {
      "callsign" : this.callsign,
      "isSpecialCallsign": false,
      "grid" : "",
      "rig" : [],
      "antenna" : [],
      "hamqth" : { "username":"callsign", "password":"password" },
      "eqsl" : { "username":"callsign", "password":"password", "msg":"tnx for qso. 73 GL" },
      "pageSize" : "Letter"
  }
  fs.writeFile(path.join(userDataPath, './config.json'), JSON.stringify(this.config, null, 2) , 'utf8', ()=>{
    console.log("config file writen");
  } ); // write it back

  // open window
  //this.mainWindow = new BrowserWindow({width: 1200, height: 800});
  this.loadURL(url.format({
    pathname: path.join(__dirname,'..','..', 'index.html'),
    protocol: 'file:',
    slashes: true,
    query: {callsign: this.callsign}
  }))
  this.mainWindow.webContents.openDevTools()

});

After(function() {
   if (this.mainWindow)
     this.mainWindow.close()
});
