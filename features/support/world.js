// features/support/world.js
const { setWorldConstructor, setDefaultTimeout, Before } = require('cucumber')
var electron = require("electron")
var app = require('electron').app
const fetch = require("node-fetch");
const { ipcMain } = require('electron')

class World {
  constructor(){
  }

  typeText(window, id, text) {
    text = text.replace(/\n/g, "\\n"); // fix multiline textarea
    return new Promise(function(resolve ,reject) {
      var code = `var input = document.querySelector("[name='${id}']");
        if (!input) {
          'FAIL: An input element with identifier ${id} was not found';
        } else {
          input.value = '${text}';
        }`
      window.webContents.executeJavaScript(code, true, function(result) {
        succeedOrFail(result, resolve, reject);
      });
    });
  };
}

var succeedOrFail = function (result, resolve, reject) {
 if (result.startsWith('FAIL:')) {
   reject(new Error(result.substring(5).trim()));
 } else {
   resolve(result);
 }
}


setWorldConstructor(World)
