
# UBerlog

this is a simple to use, no frills amateur radio logbook.
just a slick UI

read about it on my [blog](http://idoroseman.com/?p=870)

lastest version is at the [download page](https://bitbucket.org/roseman/uberlog/downloads/)

check out the [wiki](https://bitbucket.org/roseman/uberlog/wiki/Home) or [contact me](mailto:ido.roseman@gmail.com) if you need help setting it up

73

ido 4x6ub
