var path = require('path');

module.exports = {
   entry: [
     './app/index.js'
   ],
   output: {
     path: __dirname,
     filename: 'bundle.js'
   },
   module: {
     loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          include: path.join(__dirname, 'app'),
          loaders: 'babel-loader',
          query: {
            presets: [ 'es2016', 'react' ]
          }
        },
        {
          test: /\.css$/,
          loaders: 'style-loader!css-loader'
        },
        {
          test: /\.less$/,
          loader: 'less-loader', // compiles Less to CSS
        },
        { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
        { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },

      ]
    },
    target: 'node'
//    node: {
//      fs: 'empty'
//    }
  }
// Read more at https://www.pluralsight.com/guides/front-end-javascript/getting-started-with-webpack-part-1?aid=7010a000002BWqGAAW&promo=&oid=&utm_source=google&utm_medium=ppc&utm_campaign=EMEA_Dynamic&utm_content=&utm_term=&gclid=CjwKCAiA0IXQBRA2EiwAMODil-zXlCeas-vX8uhZesWQLx7Aq1JXx2JG_PfHW7ms35mzzVCikIeM-RoC9SAQAvD_BwE#V3CMGf12JygKCq68.99
