
import React from 'react';

class ModalTextbox extends React.Component {
  constructor(props){
    super(props)
    this.state = {value:props.value}
  }
  handleChange(event) {
    this.setState({value: event.target.value});
  }
  onSelect(event){
    this.props.onSelect(this.state.value)
  }
  render() {
    return <div id="myModal" className="modal" style={{"display" : this.props.show ? "block":""}}>
            <div className="modal-content">
              <span className="close" onClick={this.props.onClose}>&times;</span>
              <p>{this.props.text}</p>
              <input type="text" id="inputbox" value={this.state.value} onChange={this.handleChange.bind(this)} />
              <button onClick={this.onSelect.bind(this)}><i className="fa fa-check"></i></button>
            </div>
          </div>
        }
}

export {ModalTextbox}
