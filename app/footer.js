import React, { Component } from 'react';
var moment = require('moment');
const electron = window.require("electron")
const appVersion = (electron.app || electron.remote.app).getVersion();

export default class extends Component {
  constructor(props){
    super(props)
  }
  componentWillUnmount(){
  }

  render() {
    var rv = []
    for (var i in this.props.stat){
      rv.push(" | ")
      rv.push(<span key={i} className={"stat-" + this.props.stat[i]}>{i}</span>)
    }
    return (
      <div>
      QSO Count {this.props.count} {rv} | Version {appVersion}
      </div>
    );
  }
}
