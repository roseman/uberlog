import React from 'react';

class ElectronRouter extends React.Component {
    constructor(props) {
      super(props);
      // first parameter determins which component to draw
      var urlParams = new URLSearchParams(window.location.search);
      for(var key of urlParams.keys()) {
        if (!this.activePath)
          this.activePath = key;
      }
      // build components lookup table
      var components = {}
      this.props.children.map((child)=>{
        components[child.props.path] = child.props.component
        if (child.props.default)
          this.defaultComponent = child.props.component
      });

      this.activeComponent = this.activePath ? components[this.activePath] : this.defaultComponent
      console.log('router:', this.activePath, '=>', this.activeComponent);
    }
    render(){
      return this.activeComponent
    }
}


class ElectronRoute extends React.Component {

}

export {ElectronRoute, ElectronRouter}
