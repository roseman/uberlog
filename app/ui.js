import React, { Component } from 'react';

class Tab extends Component {
  render(){
    return (this.props.children);
  }
}

class Tabs extends Component {
  constructor(props){
    super(props);
    this.state = {selected: 0};
    this.items = [];
    for (var i in this.props.children)
      this.items.push(this.props.children[i].props.name);
  }
  handleSelect(event){
    console.log(event.target);
    this.props.onClick(event.target)
    this.setState({selected:event.target.id})
  }
  render(){
    var tabs = this.items.map((t,i)=>{
      return(<button key={i} id={i} className={this.state.selected==i?"tablink active":"tablink"} onClick={this.handleSelect.bind(this)}>{t}</button>)});
    return (
      <div>
      <div className="tabs">
        {tabs}
      </div>
      <div className="tab-area">
        {this.props.children[this.state.selected]}
      </div>
      </div>
    );
  }
}

class DropdownButton extends Component {
    constructor(props){
      super(props);
    }

    handleClick(event, text, func){
      if (func)
        func(event)
      document.getElementById("dropdown-"+this.props.title).classList.toggle("show");
    }

    render(){
      const { children } = this.props;
      var childrenWithProps = React.Children.map(children, child =>
        React.cloneElement(child, { onCascade: this.handleClick.bind(this) }));
      let isPullRight = this.props.pullRight ? " dropdown-pull-right":""
      return (
        <div className="dropdown" >
          <button onClick={this.handleClick.bind(this)} className="dropbtn" >
            <i className={"fa "+this.props.icon+" dropbtn"} aria-hidden="true"></i>
            <span style={{"fontSize":this.props.largeText?"150%":"100%"}}> {this.props.title} </span>
            <i className="fa fa-caret-down dropbtn" aria-hidden="true"></i>  
          </button>
          <div id={"dropdown-"+this.props.title} className={"dropdown-content" + isPullRight}>
            {childrenWithProps}
          </div>
        </div>
      )
    }
}

class DropdownButtonItem extends Component {
    render(){
      return  <a href="#" onClick={ (e) => this.props.onCascade(e, this.props.children, this.props.onClick) }>{this.props.children}</a>
    }
}


// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

export {Tab, Tabs, DropdownButton, DropdownButtonItem}
