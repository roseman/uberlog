import React, { Component } from 'react';
import dxcc from './dxcc';

var moment = require('moment');

export default class extends Component {
  constructor(props){
    super(props)
    this.state = { sortField:"timestamp_", sortOrder:-1 }
  }

  headerClicked(e) {
    var field = "";
    switch(e.target.innerText.trim()) {
      case "Date": field="timestamp_"; break;
      case "Callsign": field="CALL"; break;
      case "Country": field="country_"; break;
      case "Signal\nHis": field="RST_SENT"; break;
      case "Signal\nMine": field="RST_RCVD"; break;
      case "Freq": field="FREQ"; break;
      case "Mode": field="MODE"; break;
      case "Name": field="NAME"; break;
    }
    console.log(e.target.innerText, ",", field, ",", this.state.sortField, ",", this.state.sortOrder);

    if (field != "") {
      if (field==this.state.sortField)
        this.setState({sortOrder:-this.state.sortOrder})
      else
        this.setState({sortField:field});
    }
  }

  humanize(x){
    return x.toFixed(6).replace(/\.?0*$/,'');
  }

  humanFreq(freq)
  {
    let f = Math.floor(freq)
    if (f>1000)
      return (f/1000).toFixed(2).replace(/\.?0*$/,'')+"G";
    return f
  }

  render() {
    var updatedList = this.props.logbook;
    if (this.props.filterText) {
      var fieldList = ["CALL", "FREQ", "MODE", "NAME", "QTH", "GRID", "COMMENT", "MY_CITY"];
      updatedList = updatedList.filter((item)=>{
          var isMatch = false;
          fieldList.map((fieldName)=>{
            if ((item[fieldName]) && (item[fieldName].toString().toLowerCase().search(this.props.filterText.toLowerCase()) !== -1))
              isMatch = true;
          })
          if (moment.utc(item["timestamp_"]).locale("en-gb").format("L").toLowerCase().search(this.props.filterText.toLowerCase()) !== -1)
            isMatch = true;
          if (item["country_"].toLowerCase().includes(this.props.filterText.toLowerCase()))
            isMatch = true;
          return isMatch;
        });
      }
    updatedList = updatedList.sort((a,b)=>{return a[this.state.sortField] > b[this.state.sortField] ? this.state.sortOrder : -this.state.sortOrder});
    var qsomsg = ""
    if (this.props.isLoaded === false)
      qsomsg = "loading QSO's"
    else if (this.props.logbook.length == 0)
      qsomsg = "No QSO's yet. go make some";
    else if (updatedList.length == 0)
      qsomsg = "No QSO's matching your search"
    else
      qsomsg = "";

    var qsolist = updatedList.map((qso, i)=>{
      var datetime = moment.utc(qso["QSO_DATE"] + " " + qso["TIME_ON"] , "YYYYMMDD HHmm").locale("en-gb")
      var remarks = [];
      if (qso["SAT_NAME"]) remarks.push("📡")
      if (qso["COMMENT"]) remarks.push(qso["COMMENT"])
      if (qso["MY_RIG"]) remarks.push(qso["MY_RIG"])
      if (qso["MY_ANTENNA"]) remarks.push(qso["MY_ANTENNA"])
      if (qso["MY_CITY"]) remarks.push(qso["MY_CITY"])
      if (qso["MY_NAME"]) remarks.push("🎤"+qso["MY_NAME"])
      var hasqslview = false;
      if (('_attachments' in qso) && ('eqsl' in qso['_attachments']))
        hasqslview = true;
      var has_picture_qsl = !((qso["QSL_RCVD"]==undefined) || ((qso["QSL_RCVD_VIA"]==undefined) && (qso["APP_LoTW_MODEGROUP"]!=undefined)))
      let has_lotw_qsl = qso["APP_LoTW_MODEGROUP"] !== undefined
      let has_qrzcom_qsl = qso["APP_QRZLOG_STATUS"] == "C"
      return(
        <tr key={i}>
          <td>{datetime.format("L")}</td>
          <td>{datetime.format("LT")}</td>
          <td>{qso["CALL"]}</td>
          <td>{qso["country_"]}</td>
          <td>{qso["RST_SENT"]}</td>
          <td>{qso["RST_RCVD"]}</td>
          <td>{"FREQ_RX" in qso ? this.humanFreq(qso["FREQ"])+"/"+this.humanFreq(qso["FREQ_RX"]) : this.humanFreq(qso["FREQ"])}</td>
          <td>{"SUBMODE" in qso ? qso["SUBMODE"]:qso["MODE"]}</td>
          <td>{qso["NAME"]}</td>
          <td>{qso["QTH"] ? qso["QTH"] : qso["GRID"]}</td>
          <td style={{"overflow": "hidden", "whiteSpace": "nowrap"}}>
            <input id={qso["_id"]} type="checkbox" checked={qso["QSL_SENT"]=="Y"} onChange={this.props.onSentClick}/>
            { qso["QSL_SENT"]!="Y"?<a id={qso["_id"]} href="#" onClick={this.props.onSendEQsl}>Send</a>:""}
          </td>
          <td style={{"overflow": "hidden", "whiteSpace": "nowrap"}}>
              <div id={"qsl"+qso["_id"]} className="spnDetails">
                <input id={qso["_id"]} type="checkbox" checked={has_picture_qsl || has_lotw_qsl || has_qrzcom_qsl} onChange={this.props.onRcvdClick}/>
                {/*}
                <span id={"tooltip"+qso["_id"]} className="spnTooltip">
                  <label className="container">Paper
                    <input id={"paper"+qso["_id"]} type="checkbox" checked={false} readOnly/>
                    <span className="checkmark"></span>
                  </label>
                  <br/>
                  <label className="container">eqsl
                    <input id={"eqsl"+qso["_id"]} type="checkbox" checked={has_picture_qsl} readOnly/>
                    <span className="checkmark"></span>
                  </label>
                  <br/>
                  <label className="container">lotw
                    <input id={"lotw"+qso["_id"]} type="checkbox" checked={has_lotw_qsl} readOnly/>
                    <span className="checkmark"></span>
                  </label>
                  <br/>
                  <label className="container">qrz.com
                    <input id={"lotw"+qso["_id"]} type="checkbox" checked={has_qrzcom_qsl} readOnly/>
                    <span className="checkmark"></span>
                  </label>
                </span>
                {*/}
                {hasqslview?<a id={qso["_id"]} href="#" onClick={this.props.onShowEQsl}>view</a>:""}
              </div>
          </td>
          <td>{remarks
            .map((item, index) => (<span key={index}>{item}</span>))
            .reduce((acc, x) => acc === null ? [x] : [acc, '|', x], null)
          }</td>
          <td><i className="fa fa-pencil-square-o" aria-hidden="true" id={qso["_id"]} onClick={this.props.onEditBox}></i>
          <i className="fas fa-satellite-dish"></i></td>

        </tr>
        )
      }
    )
    var sortIcon = this.state.sortOrder > 0 ? <i className="fa fa-caret-up" aria-hidden="true"></i> : <i className="fa fa-caret-down" aria-hidden="true"></i>;
    return (
      <div className="logbook-outer-container">
        <div className="logbook-container">
            <table id="logbook-table" className="table-list scrollable">
            <tbody>

          <tr>
          <th scope="col" onClick={this.headerClicked.bind(this)}>Date {this.state.sortField=="timestamp_"?sortIcon:""}</th>
          <th scope="col">Time</th>
          <th scope="col" onClick={this.headerClicked.bind(this)}>Callsign {this.state.sortField=="CALL"?sortIcon:""}</th>
          <th scope="col" onClick={this.headerClicked.bind(this)}>Country {this.state.sortField=="country_"?sortIcon:""}</th>
          <th scope="col" onClick={this.headerClicked.bind(this)}>Signal<br/>His {this.state.sortField=="RST_SENT"?sortIcon:""}</th>
          <th scope="col" onClick={this.headerClicked.bind(this)}>Signal<br/>Mine {this.state.sortField=="RST_RCVD"?sortIcon:""}</th>
          <th scope="col" onClick={this.headerClicked.bind(this)}>Freq {this.state.sortField=="FREQ"?sortIcon:""}</th>
          <th scope="col" onClick={this.headerClicked.bind(this)}>Mode {this.state.sortField=="MODE"?sortIcon:""}</th>
          <th scope="col" onClick={this.headerClicked.bind(this)}>Name {this.state.sortField=="NAME"?sortIcon:""}</th>
          <th scope="col">QTH</th>
          <th scope="col">S</th>
          <th scope="col">R</th>
          <th scope="col">Remarks</th>
          <th scope="col"></th>
          </tr>
          {qsomsg == ""  ? qsolist:<tr></tr> }
        </tbody>
      </table>
      {qsomsg != ""  ? <center>{qsomsg}</center>:"" }
      </div>
      </div>
    );
  }
}
