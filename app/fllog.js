var net = require('net');
const EventEmitter = require('events');

class fllog extends EventEmitter{
  // http://www.n3fjp.com/help/api.html

  constructor(username, password){
    super();

    this.server = net.createServer();
    this.server.on('connection', this.handleConnection.bind(this));
    this.server.on('error', (err)=>{
      console.log("Socket error: " + err);
    })
    this.dictionary = {
      "COMMENTS":"COMMENT",
      "DATE":"QSO_DATE",
      "FREQUENCY":"FREQ",
      "NAMER":"NAME",
      "QTHGROUP":"QTH",
      "RSTR":"RST_RCVD",
      "RSTS":"RST_SENT",
      "TIMEOFF":"TIME_OFF",
      "TIMEON":"TIME_ON",
    }
    this.server.listen(1100, () => {
      console.log('fldigi server listening to port ', this.server.address().port);
    });
  }

  handleConnection(conn) {
    this.conn = conn;
    var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
    console.log('new client connection from %s', remoteAddress);
    this.qso = {}

    conn.on('data', this.onConnData.bind(this));
    conn.on('close', this.onConnClose.bind(this));
    conn.on('error', this.onConnError.bind(this));
    this.emit('status',{"fldigi":"idle"})

  }

  onConnData(d) {

    function extractTag(line,tag){
      var p = line.indexOf("<"+tag+">")+tag.length+2;
      var q = line.indexOf("</"+tag+">");
      var a = line.substr(p,q-p);
      return a;
    }
//      console.log('connection data from %s: %j', remoteAddress, d);
//      conn.write(d);
    this.emit('status',{"fldigi":"active"})

    var text = "";
    for (var i = 0; i < d.length; i++) {
      text += String.fromCharCode(d[i]);
    }

    var lines = text.split("\r\n");
    for (var line in lines) {
      if (lines[line] == "")
        continue;

      var cmnd = lines[line].substr(5,lines[line].length-11);

      if (cmnd == "<PROGRAM>") {
        // what software am i
        this.conn.write("<CMD><PROGRAMRESPONSE><PGM>UBerLog Amateur Contact Log</PGM><VER>1.0</VER><APIVER>0.6.2</APIVER></CMD>\r\n")
      }
      else if (cmnd=="<RIGENABLED>"){
        // does logbook control rig ? returns rig name
        this.conn.write("<CMD><RIGRESPONSE><RIG>None</RIG></CMD>")
      }
      else if (cmnd.startsWith("<SENDRIGPOLL>")) {
        var freq = extractTag(cmnd,"FREQ");
        var mode = extractTag(cmnd,"MODE");
      }
      else if (cmnd.startsWith("<ACTION><VALUE>")){
        var action = extractTag(cmnd, "VALUE");
        console.log("ACTION", action);
        if (action=="CLEAR"){
            this.qso = {}
        }
        else if (action=="CALLTAB"){
          this.emit("callsign", this.qso)
        }
        else if (action=="ENTER"){
          this.emit("qso", this.qso)
        }
      }
      else if (cmnd.startsWith("<UPDATE><CONTROL>")){
        var control = extractTag(cmnd, "CONTROL");
        var value = extractTag(cmnd, "VALUE");
        if (control.startsWith("TXTENTRY")){
          console.log("trunkating control", control);
          control=control.substr("TXTENTRY".length);
        }
        if (control == "OTHER8"){
          value.split(' ').map((segment)=>{
            var items = segment.split(":")
            this.qso[items[0]] = items[1]
          })
        }
        else
        {
          if (control=="DATE")
            value = value.replace(/\//g, "");
          if (control.startsWith("TIME"))
          value = value.replace(/\:/g, "");
          if (control in this.dictionary)
            control = this.dictionary[control]
          if (value != "")
            this.qso[control]=value;
        }
        console.log("Control",control, value)
      }
      else if (cmnd.startsWith("<CHANGEBM>")){
        this.qso["MODE"] = extractTag(cmnd, "MODE").upper();
        this.qso["BAND"] = extractTag(cmnd, "BAND") + "m";
      }
      else
        console.log("unknown", cmnd );
      /*
      <CALLTABENTEREVENTS><VALUE>TRUE</VALUE>
      <READOFFSETENABLED>
      <<CMD><NEXTSERIALNUMBER></CMD>>
      <READOFFSET>
      <READMODEDEFAULTSUPPRESS>
      <READBMF>
      */
    }
    this.emit('status',{"fldigi":"idle"})

  }

  onConnClose() {
    console.log('connection from %s closed', this.remoteAddress);
    this.emit('status',{"fldigi":"disabled"})

  }

  onConnError(err) {
    console.log('Connection %s error: %s', this.remoteAddress, err.message);
    this.emit('status',{"fldigi":"error"})

  }
}

export {fllog}
