import React, { Component } from 'react';
var moment = require('moment');
import { DropdownButton, DropdownButtonItem} from './ui'
const {ipcRenderer} = window.require('electron')

//------------------------------------------------------------------------------
//                             L E F T
//------------------------------------------------------------------------------
class HorizontalRule extends Component {
  render() {
    return <hr/>
  }
}

class InputToolbar extends Component {
  constructor(props){
    super(props)
    this.bands = {
      "1.8":"160m",
      "3.5":"80m",
      "7":"40m",
      "10":"30m",
      "14":"20m",
      "18":"17m",
      "21":"15m",
      "25":"12m",
      "28":"10m",
      "50":"6m",
      "144":"2m",
      "220":"1.25m",
      "432":"70cm",
      "902":"35cm",
      "1300":"23cm",
      "2300":"13cm",
      "3300":"9cm",
      "5660":"6cm",
      "10000":"3cm",
      "24000":"1.25cm",
      "47000":"6mm",
      "75000":"4mm",
      "120000":"2.5mm",
      "142000":"2mm",
      "241000":"1mm"
    }
    this.freqs = [];
    for (var f in this.bands)
          this.freqs.push(<option value={f} key={f} >{f>1000?(f/1000)+" GHz":f+" MHz"} / {this.bands[f]} </option>);
    this.freqs.sort((a,b) => { return parseInt(a.key) - parseInt(b.key)});
  }

  render() {
    var rigs = this.props.rigs.map((rig,i )=>{return <option key={i} value={rig.replace(" ","_")}>{rig}</option>});
    rigs.unshift(<option key="999" value="">None</option>);
    var antennas = this.props.antennas.map((ant,i )=>{return <option key={i} value={ant.replace(" ","_")}>{ant}</option>});
    antennas.unshift(<option key="999" value="">None</option>);
    return (
      <div>
      <select id="freq" value={this.props.freq} onChange={this.props.onFreqChanged} disabled={this.props.sat!=""}>{this.freqs}</select>
      {" "}
      <select id="mode" value={this.props.mode} onChange={this.props.onModeChanged}>
        <option value="SSB">SSB</option>
        <option value="SSB|USB">USB</option>
        <option value="SSB|LSB">LSB</option>
        <option value="CW">CW</option>
        <option value="RTTY">RTTY</option>
        <option value="AMTOR">AMTOR</option>
        <option value="PKT">PKT</option>
        <option value="AM">AM</option>
        <option value="FM">FM</option>
        <option value="SSTV">SSTV</option>
        <option value="ATV">ATV</option>
        <option value="PACTOR">PACTOR</option>
        <option value="CLOVER">CLOVER</option>
        <option value="PSK|PSK31">PSK31</option>
        <option value="PSK|PSK63">PSK63</option>
        <option value="JT65">JT65</option>
        <option value="JT9">JT9</option>
        <option value="FT8">FT8</option>
        <option value="MFSK|JS8">JS8Call</option>
        <option value="DIGITALVOICE|FreeDV">FreeDV</option>
      </select>
      {" "}
      <select id="sat" value={this.props.sat} onChange={this.props.onSatChanged}>
        <option value="">Satellite</option>
        <option value="QO-100|SX">QO-100</option>
        <option value="ARISS|UV">ARISS</option>
        <option value="SO-50|VU">SO-50</option>
        <option value="A0-91|UV">AO-91</option>
        <option value="AO-92|UV">AO-92</option>
      </select>
      <br/>
      <br/>
      <select id="rig" value={this.props.rig} onChange={this.props.onSomethingChanged}>
      {rigs}
      </select>
      {" "}
      <select id="antenna" value={this.props.antenna} onChange={this.props.onSomethingChanged}>
      {antennas}
      </select>
      {" "}
      <input type="text" id="city" name="city" size="12" placeholder="location..." value={this.props.city} onChange={this.props.onSomethingChanged} />
      <br/>
      <br/>
      </div>
    );
  }
}

//------------------------------------------------------------------------------
//                              R I G H T
//------------------------------------------------------------------------------

class QsoListToolbar extends Component {
  constructor(props){
    super(props)
    this.intervalId = setInterval(this.timerTick.bind(this), 1000);
    this.state = {datetime:""}
  }
  componentWillUnmount(){
    clearInterval(this.intervalId);
  }
  timerTick(){
    this.locale = moment().locale("en_gb");         // en
    this.setState({datetime:this.locale.utc().format("l LT")});
  }
  searchChanged(event){
    this.props.onSearch(event.target.value);
  }
  handleStayontop(event){
	        ipcRenderer.send('always-on-top', event.target.checked)
  }

  render() {
    var calls = []
    for (var i in this.props.callsigns) {
      if (this.props.callsigns[i] != this.props.callsign)
        calls.push(<DropdownButtonItem key={i} value={this.props.callsigns[i]} onClick={this.props.onChangeLogbook}>{this.props.callsigns[i]}</DropdownButtonItem>)
    }
    var location = "";
    if (this.props.geodata){
      location = " | " + this.props.geodata.latitude+"/"+this.props.geodata.longitude+" - "+this.props.geodata.city
    }
    return (
      <div>
      <div className="right_align">
        <DropdownButton title={this.props.callsign.toUpperCase()} icon="" largeText={true} pullRight={true}>
          <DropdownButtonItem onClick={this.props.onSpecialCallsign}>Special Callsign...</DropdownButtonItem>
          <DropdownButtonItem onClick={this.props.onGuestOperator}>Guest Opertor</DropdownButtonItem>
          <HorizontalRule/>
          {calls}
          <HorizontalRule/>
          <DropdownButtonItem onClick={this.props.onSettings}>Settings...</DropdownButtonItem>
        </DropdownButton>
      </div>
      <font face="verdana" >
      {this.state.datetime}
      {location}
      {" " + this.props.grid}
      {" "}{typeof this.props.temprature !== 'undefined' ? [this.props.temprature, <span key="degc">&deg;C</span>] : ""}
      </font>
      <br/>
      <br/>
      <button onClick={this.props.onEqslSync}><i className="fa fa-refresh" aria-hidden="true"></i> eQSL</button>
      <button onClick={this.props.onLoTWSync}><i className="fa fa-refresh" aria-hidden="true"></i> LoTW</button>
      <button onClick={this.props.onQRZComSync}><i className="fa fa-refresh" aria-hidden="true"></i> qrz.com</button>

      {" | "}
      <button onClick={this.props.onMap}><i className="fa fa-globe" aria-hidden="true"></i> Grid Map</button>

      {" | "}
      <DropdownButton title="Export" icon="fa-download">
        <DropdownButtonItem onClick={this.props.onExportPDF}>PDF</DropdownButtonItem>
        <DropdownButtonItem onClick={this.props.onExportAdif}>Adif</DropdownButtonItem>
      </DropdownButton>
      <DropdownButton title="Import" icon="fa-upload">
        <DropdownButtonItem onClick={this.props.onImportAdif}>Adif</DropdownButtonItem>
      </DropdownButton>
      {" | "}
      <input type="text" id="logfilter" className="" placeholder="Search" onChange={this.searchChanged.bind(this)}/>
      <button onClick={(event)=>{document.getElementById('logfilter').value = ""; this.searchChanged(event)}}><i className="fa fa-times"></i></button>
      {" | "}
	  <input type="checkbox" id="stayontop" name="stayontop" onClick={this.handleStayontop.bind(this)} />
	  <label htmlFor="stayontop">Stay on top</label>
      </div>
    );
  }
}

export {InputToolbar, QsoListToolbar}
