var fs = require('fs');
var http = require('http');
var path = require('path')
const electron = window.require("electron")
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var fullpath = path.join(userDataPath, "dxcclist.txt")
var url = "http://www.arrl.org/files/file/DXCC/2017-4-7%20DXCC%20List.txt";


String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

String.prototype.IncrementAt=function(index) {
    if (index == -1)
      index = this.length - 1;
    var replacement = String.fromCharCode(this[index].charCodeAt() + 1)
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

class dxcc {
  constructor(){
    this.list = null;
  }

  downloadFromServer(){
    return new Promise((resolve, reject) => {
       console.log("downloading dxcc...");
       //    request('http://google.com/doodle.png').pipe(fs.createWriteStream('doodle.png'))
       var file = fs.createWriteStream(fullpath);
       var request = http.get(url, function(response) {
         response.pipe(file);
         resolve();
       });
     });
  }

  differAtIndex(a, b){
    var shorterLength = Math.min(a.length, b.length);

    for (var i = 0; i < shorterLength; i++)
    {
        if (a[i] !== b[i]) return i;
    }

    if (a.length !== b.length) return shorterLength;

    return -1;
  }

  loadFromFile(){
	console.log("loading dxcc list from file " + fullpath);
    return new Promise((resolve, reject) => {
      let self = this;
      fs.readFile(fullpath, 'utf-8', (err, data) => {
        if(err)
          reject("An error ocurred reading the file: " + filename + "\n"+ err.message);
        console.log("reading dxcc");
        self.list = {};
        var lines = data.split('\n');
        var index = 0;
        //skip header
        while ((lines[index]===undefined) || (!lines[index].includes("_"))) {
          index++;
        }


        while (!lines[index++].toLowerCase().includes("notes:")) {
          var line = lines[index];
          if ((line===undefined) || (line.trim() == ""))
              continue;
          var prefix = line.substr(4, 20).replace("*", "").replace("#", "").trim();
          var name = line.substr(24, 35).replace("Asiatic","As.").replace("European","Eu.").replace("Federal Republic of ","F.R.").replace("United States of America","USA").replace("Republic","Rep.").trim();
          var continent = line.substr(59, 2);
          var itu = line.substr(65, 2);
          var cq = line.substr(71, 2);
          var code = line.substr(77, 2);
          if (prefix.endsWith(")"))
              prefix = prefix.substr(0, prefix.indexOf('('));
          var prefixes = prefix.split(',');
          prefixes.forEach((p)=>{
            var count = p.split('-').length - 1;
            if (count == 2) {
              var fromto = p.split('-');
              var from1 = fromto[0];
              var to1 = fromto[1].substr(0,from1.length);
              var from2 = fromto[1].substr(from1.length);
              var to2=fromto[2];
              to1 = to1.IncrementAt(-1);
              to2 = to2.IncrementAt(-1);
              for (var p1 = from1; p1!=to1; p1=p1.IncrementAt(-1))
                for (var p2 = from2; p2!=to2; p2=p2.IncrementAt(-1))
                  self.list[p1+p2] = name;
            }
            else if (count == 1) {

              var suffix = "";
              var fromto = p.split('-');
              if (fromto[0].length > fromto[1].length)
                fromto[1] = fromto[0].substr(0,fromto[0].length - fromto[1].length) + fromto[1];
              if (fromto[1].length > fromto[0].length){
                     suffix = fromto[1].substr(fromto[0].length);
                     fromto[1] = fromto[1].substr(0, fromto[0].length);
              }
              var loc = this.differAtIndex(fromto[0],fromto[1]);
              fromto[1] = fromto[1].IncrementAt(loc);
              for (var p1 = fromto[0]; p1 != fromto[1]; p1=p1.IncrementAt(loc))
                if (!(p1+suffix in self.list)){
                  self.list[p1+suffix] = name;
                }

            }
            else if (p) {
              if (!(p in self.list))
                self.list[p] = name;
            }

          })
        }
        self.list["R"] = "Russia"; // hack: this is missing from file
        self.list["1B"] = "N. Cyprus";
        self.list["1S"] = "Spratly Is";
        self.list["2E"] = "England (Novices)";
        self.list["2I"] = "Northern Ireland (Novices)";
        self.list["2J"] = "Jersey (Novices)";
        self.list["2M"] = "Scotland (Novices)";
        self.list["2U"] = "Guernsey & Dependencies (Novices)";
        self.list["2W"] = "Wales (Novices)";
        console.log("done reading dxcc");
        resolve();
      }) // of readFile
    }) // of Promise
  }

  load()
  {
    var p = new Promise((resolve, reject) => {
      if (fs.existsSync(fullpath))
         this.loadFromFile().then(()=>{resolve();})
      else
        this.downloadFromServer().then(this.loadFromFile).then(()=>{resolve();})
      });
    //p.then(()=>{ parent.forceUpdate(); });
    return p;
  }

  countryOf(callsign)
  {
    if (this.list===null)
      return "";
    if (!(callsign))
      return ""

    while (callsign != "")
    {
      if (callsign in this.list)
        return this.list[callsign];
      callsign = callsign.slice(0, -1);
    }

    return "";
  }
}

module.exports = new dxcc()
