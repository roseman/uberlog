import React, { Component } from 'react';
var moment = require('moment');
import dxcc from './dxcc';
import {hamqth} from './hamqth';

var nato = {
  1: 'One',
  2: 'Two',
  3: 'Three',
  4: 'Four',
  5: 'Five',
  6: 'Six',
  7: 'Seven',
  8: 'Eight',
  9: 'Nine',
  0: 'Zero',
	A: 'Alpha',
	B: 'Bravo',
	C: 'Charlie',
	D: 'Delta',
	E: 'Echo',
	F: 'Foxtrot',
	G: 'Golf',
	H: 'Hotel',
	I: 'India',
	J: 'Juliet',
	K: 'Kilo',
	L: 'Lima',
	M: 'Mike',
	N: 'November',
	O: 'Oscar',
	P: 'Papa',
	Q: 'Quebec',
	R: 'Romeo',
	S: 'Sierra',
	T: 'Tango',
	U: 'Uniform',
	V: 'Victor',
	W: 'Whiskey',
	X: 'X-ray',
	Y: 'Yankee',
	Z: 'Zulu'
};

export default class extends Component {
  constructor(props) {
    super(props);
    this.epoch = moment.utc(0);
    this.hamqth = new hamqth("4x6ub","12345678");
    this.callsign = "";
    this.qrz = {};
    this.state = { text:"", callsign:"", his:"", mine:"", name:"", qth:"", grid:"", notes:"", date:"", time:""}
    this.myRef = React.createRef();
  }
  handleSubmit(event){
    var success = this.props.onSubmit(this.state);
    console.log("submit success is", success);
    if (success){
      this.setState({text:""});
    }
  }
  handleKeyPress(event){
    //console.log(event.key,event.which, event.keyCode, event.charCode, event.metaKey);
    if ((event.metaKey) && (event.keyCode == 8)){
      this.setState({text:""});
      //this.handleInput(event);
    }
    else if (event.keyCode == 9) {
      var size = Object.keys(this.qrz).length;
      if (size > 0) {
        var pos = event.target.selectionEnd;
        if (this.state.text[pos-1] == " ")
          pos--;
        if (this.state.text[pos-2] == "\n") {
          event.preventDefault();
          if (this.state.text[pos-1] == "n")
             this.setState({text:this.state.text.substr(0,pos)+" "+this.qrz.name});
          else if (this.state.text[pos-1] == "q")
            this.setState({text:this.state.text.substr(0,pos)+" "+this.qrz.qth});
          else if (this.state.text[pos-1] == "g")
            this.setState({text:this.state.text.substr(0,pos)+" "+this.qrz.grid});
        }
      }
      console.log("Tab", event.target.selectionStart, event.target.selectionEnd);
    }
  }

  handleInput(event){
    var s = { callsign:"", his:"", mine:"", name:"", qth:"", grid:"", comment:"", date:"", time:""}
    this.setState({text:this.myRef.current.value})
    this.myRef.current.value.split('\n').forEach((line,i)=>{
      if ((this.isCallsign(line)) && !(s.callsign))
        s.callsign = line.toUpperCase().trim();
      else if (this.isSignalReport(line)) {
        if (!s.his)
          s.his=line;
        else if (!s.mine)
          s.mine=line;
        else
          console.log("unprocessed signal report", line);
        }
      else if (this.isGrid(line))
        s.grid = line.substr(0,2).toUpperCase() + line.substr(2).toLowerCase();
      else if (this.isDate(line) || this.isTime(line)) {
          line.split(" ").map((token)=>{
          if (this.isDate(token))
            s.date = this.extractDate(token);
          if (this.isTime(token))
            s.time = this.extractTime(token);
          });
        }
      else if (this.isFreq(line))
        s.freq = line;
      else if ((line.toLowerCase().startsWith("h")) && (this.isSignalReport(line.substr(1))))
        s.his = line.substr(1).trim();
      else if ((line.toLowerCase().startsWith("m")) && (this.isSignalReport(line.substr(1))))
        s.mine = line.substr(1).trim();
      else if (line.toLowerCase().startsWith("n ")) // explisit name
        s.name = line.substr(1);
      else if (line.toLowerCase().startsWith("q ")) // explisit qth
        s.qth = line.substr(1);
      else if (line.toLowerCase().startsWith("g ")) // explisit qth
        s.grid = line.substr(1).trim().substr(0,2).toUpperCase() + line.substr(1).trim().substr(2).toLowerCase();
      else if (line != "")
        s.comment += line

      this.setState(s);
      if(s.callsign != this.callsign)
      {
        this.hamqth.lookup(s.callsign.trim())
        .then((info)=>{this.qrz = info; this.forceUpdate();})
        .catch((err)=>{
			console.log("hamqth:"+err); 
			if (err.split(":",2)[0] == this.callsign)
			  this.qrz={}
			})
        this.callsign = s.callsign.trim();
        this.props.onNewCallsign(this.callsign);
      }
    })
    if (this.myRef.current.value.endsWith('\n\n\n')){
      var success = this.props.onSubmit(s);
      console.log("submit success is", success);
      if (success){
        this.setState({text:""});
        //this.handleInput(event)
      }
    }
  }

  isCallsign(text){
    var callsignRe = /([a-zA-Z0-9]{1,2}\/)?[a-zA-Z0-9]{1,2}\d{1,4}[a-zA-Z]{1,4}(\/[a-zA-Z0-9]{1,2})?/g
    return (callsignRe.exec(text) !== null)
  }
  isSignalReport(text){
    var pattern1 = /^\s*[1-5][1-9][1-9]?(\+\d0)?$/g
    var pattern2 = /^\s*[\-\+]\d{1,2}$/g
    return ((pattern1.exec(text) !== null) || (pattern2.exec(text) !== null))
  }
  isGrid(text){
    return /^[a-zA-Z]{2}\d{2}([a-zA-Z]{2})?$/g.exec(text) !== null;
  }
  isDate(text){
    return /\d{1,2}\/\d{1,2}\/\d{2,4}/g.exec(text) !== null;
  }
  isTime(text){
    return /\d{1,2}\:\d\d/g.exec(text) !== null;
  }
  isFreq(text){
    return /\d{1,2}\.\d{2,3}/g.exec(text) !== null;
  }
  extractDate(text){
    var d = moment(text, ["DD/MM/YYYY", "DD/MM/YY"]);
    return d.format("YYYYMMDD")
  }
  extractTime(text){
    var d = moment(text, ['h:m a', 'H:m']);
    return d.format("HHmm")
  }

  phonetic(callsign){
      var rv = []
      for (let c of callsign)
        rv.push(nato[c])
      return rv.join(" ")
  }

  render() {
    return (
      <div>
        <center><h1>{this.state.callsign ? this.state.callsign.replace("0","Ø") : "callsign"}</h1></center>
		{
			//<center>{this.phonetic(this.state.callsign)}</center>
		}
        Report His {this.state.his ? this.state.his : "..."} Mine {this.state.mine ? this.state.mine : "..."}<br/>
        Name {this.state.name?this.state.name: (this.qrz.name?<span style={{"color":"Gray"}}>{this.qrz.name}</span>:"...")}<br/>
        QTH {this.state.qth ? this.state.qth : (this.qrz.qth?<span style={{"color":"Gray"}}>{this.qrz.qth}</span>:"...")}
        {this.state.grid ? "  Grid "+this.state.grid: (this.qrz.grid?<span> Grid <span style={{color:"Gray"}}>{this.qrz.grid}</span></span>:"")}<br/>
        Country {this.state.callsign ? dxcc.countryOf(this.state.callsign):"..."}<br/>
        {this.state.date ? "Date "+this.state.date:""}
        {this.state.time ? " Time "+this.state.time:""}
        {this.state.freq ? " Freq "+this.state.freq:""}
        <div className="right_align">
          <i className="fa fa-check" style={{"color":"Green"}} aria-hidden="true" onClick={this.handleSubmit.bind(this)}></i>
          <i className="fa fa-times" style={{"color":"Red"}} aria-hidden="true" onClick={()=>{this.setState({text:""})}}></i> 
        </div>
        <br/>
        <form>
          <textarea name="Text1" cols="30" rows="8"
              ref={this.myRef}
              value={this.state.text}
              onKeyDown={this.handleKeyPress.bind(this)}
              onKeyUp={this.handleKeyPress.bind(this)}
              onChange={this.handleInput.bind(this)}>
              </textarea>
        </form>
        <button id="process" onClick={this.handleInput.bind(this)} style={{display:"none"}}>process</button>
        <br/>
        {this.props.lastSeen===null?"":<div>
		    <b>Last seen</b> {this.props.lastSeen['MODE']}, {Math.floor(this.props.lastSeen['FREQ'])}MHz<br/>
			<b>on</b> {moment(this.props.lastSeen["timestamp_"]).format("DD/MM/YY HH:mm")}<br/>
			<a onClick={this.props.onSeeMore}>more...</a>
			</div>}
      </div>
    );
  }
}
