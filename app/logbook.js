const EventEmitter = require('events');
const PouchDB = require('pouchdb-browser');
var fs = require("fs");
var path = require('path')
var moment = require('moment');
var blobUtil = require('blob-util')
import helpers from './helpers'


const electron = window.require("electron")
const userDataPath = (electron.app || electron.remote.app).getPath('userData');

class logbook extends EventEmitter {
  constructor(callsign)
  {
    super();
    this.dxcc = null;
    this.list = [];
    this.db = new PouchDB( 'log_'+callsign ,{adapter : 'websql'})
    this.db.info().then(function (info) {
      console.log("db info",info);
    })
  }

  syncWith(username, password, siteurl, dbname){
    this.remoteDB = new PouchDB(siteurl+dbname,{auth: {
      username: username,
      password: password
    }})
    console.log("remote", this.remoteDB);
    this.db.sync(this.remoteDB, {
      live: true,
      retry: true
    }).on('change', (info) => {
      // handle change
      this.emitChange()
    }).on('paused', (err) => {
      // replication paused (e.g. replication up to date, user went offline)
      this.emit('status', {"Server":"idle"})
    }).on('active', () => {
      // replicate resumed (e.g. new changes replicating, user went back online)
      this.emit('status', {"Server":"active"})
    }).on('denied', (err) => {
      // a document failed to replicate (e.g. due to permissions)
      this.emit('status', {"Server":"error"})
    }).on('complete', (info) =>{
      // handle complete
      this.emit('status', {"Server":"active"})
    }).on('error', (err) =>{
      // handle error
      this.emit('status', {"Server":"error"})
    });

  }

  //----------------------------------------------------------------------------
  //                             API
  //----------------------------------------------------------------------------

  setDxcc(dxcc){
    this.dxcc = dxcc
  }

  getById(id){
    return new Promise((resolve, reject) => {
      this.db.get(id).then((result)=>{resolve(result)}).catch((err)=>{reject(err)})
    })
  }

  updateQso(qso){
    this.db.put(qso).then((res)=>{
      this.emitChange()
   }).catch((err)=>{console.log(qso, err);})
  }

  append(qso) {
    this.calculateQsoFields(qso);
    this.db.put(qso).then((res)=>{
      this.emitChange()
     })
   }

  injectDxcc(dxcc){
    console.log("logbook got dxcc");
    this.dxcc = dxcc
    this.updateLogbook();
    this.emitChange()
  }

  attachImageFromUrl(id, name, url){
    //var buff = new Buffer(data);
    //var blob = new Blob(['Is there life on Mars?'], {type: 'text/plain'});
    this.db.get(id).then((doc)=>{
      blobUtil.imgSrcToBlob(url).then((blob)=>{
        doc["_attachments"] = {}
        doc["_attachments"][name] =  {
          "content_type": "image/png",
          "data": blob //new Blob([data], {type: 'image/png'})
        }
        this.db.put(doc)
        .then((result)=>{console.log(result);})
        .catch((error)=>{console.log(error);})
        })
    })
    .catch((err)=>{console.log(err);})

  }
  getAttachment(id, name) {
    return this.db.getAttachment(id, name)
  }
  //----------------------------------------------------------------------------
  //                          internal
  //----------------------------------------------------------------------------

  emitChange()
  {
    this.db.allDocs({
      include_docs: true,
      attachments: true
    }).then( (result) => {
      var list = []
      result.rows.map((item)=>{list.push(item.doc)})
      this.emit('change', list)
    }).catch( (err) => {
      console.log(err);
    });
  }

  updateLogbook(){
    this.db.allDocs({
      include_docs: true,
      attachments: true
    }).then((result) => {
      result.rows.map((qso)=>{
        if (!qso.doc.hasOwnProperty("country_")) { 
          qso.doc["country_"] = this.dxcc.countryOf(qso.doc["CALL"]);
          this.db.put(qso.doc)
        }
      })
      this.emitChange()
    }).catch(function (err) {
      console.log(err);
    });

  }

  writeLogbookToFile(filename){
    this.db.allDocs({
      include_docs: true,
      attachments: false
    }).then( (result) => {
      let writeStream = fs.createWriteStream(filename);
      writeStream.write("UBerLog\n", 'utf-8');
      writeStream.write("<EOH>\n", 'utf-8')
      result.rows.map((item)=>{
        writeStream.write(helpers.objectToAdif(item.doc)+"\n", 'utf-8');
      });
      writeStream.on('finish', () => {
        console.log('wrote all data to file');
      });
      writeStream.end();
    })
  }

  loadFromFile(filepath){
        this.filename = filepath;
        fs.readFile(filepath, 'utf-8', (err, data) => {
            if(err){
                // alert("An error ocurred reading the file: " + filepath + "\n"+ err.message);
                this.emit('change',[])
                return;
            }

            this.list = helpers.parseAdifFile(data);
            this.list.forEach((qso)=>{this.calculateQsoFields(qso); });
            this.list.forEach((qso)=>{
              this.db.put(qso)
              .then(function (response) {
                // handle response
                console.log(response);
              }).catch(function (err) {
                console.log(err);
              })
            })
            this.emit('change', this.list)

        });
  }

  calculateQsoFields(qso){
    if(this.dxcc)
      qso["country_"] = this.dxcc.countryOf(qso["CALL"]) ;
    qso["timestamp_"] =  moment.utc(qso["QSO_DATE"] + " " + qso["TIME_ON"] , "YYYYMMDD HHmm");
    qso["_id"] = qso["QSO_DATE"]+"-"+qso["TIME_ON"]+"-"+qso["CALL"];
    var qslpath = path.join(userDataPath, "QSLs", qso["QSO_DATE"]+"-"+qso["TIME_ON"]+"-"+qso["CALL"]+".png");
    if (fs.existsSync(qslpath))
      qso["qslimage_"] = qslpath;
  }

}
export { logbook }
