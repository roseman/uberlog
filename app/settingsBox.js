import React, { Component } from 'react';

//const shell = require('electron').shell;
//const path = require('path')

export default class extends Component {
  constructor(props) {
    super(props);
    this.orig = JSON.stringify(this.props.settings,null,2)
    this.state = { text:this.orig, needToSave:false }
  }
  handleChange(e){
    this.setState({text:event.target.value, needToSave:true})
  }
  handleRevert(event){
    this.setState({ text:this.orig, needToSave:false })
  }
  handleEdit(event){
//    shell.openItem(path.join(userDataPath, './config.json'));
  }
  render(){
    return (
      <div>

      <button onClick={this.props.onDownload}>Download</button>
      <button onClick={this.props.onUpload}>Upload</button>
      <button onClick={this.props.onSave} disabled={!this.state.needToSave}>Save</button>
      <button onClick={this.handleRevert.bind(this)} disabled={!this.state.needToSave}>Revert</button>
      <form>
        <textarea name="Text1" cols="30" rows="20"
            value={this.state.text}
            onChange={this.handleChange.bind(this)}
        >
        </textarea>
      </form>
    </div>
    )
  }
}
