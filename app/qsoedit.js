import React from 'react';
import { logbook } from './logbook'

class QsoEditor extends React.Component {
  constructor(props) {
    super(props);

    var urlParams = new URLSearchParams(window.location.search);
    this.id = urlParams.get('qsoedit')
    this.callsign = urlParams.get('iam')
    this.state = { qso:null }
    this.logbook = new logbook(this.callsign);
    this.logbook.getById(this.id).then((qso)=>{this.setState({qso})})
  }

  render(){
    let qso = this.state.qso
    let qsl = {eqsl:false, qrzcom:false, lotw:false}
    let lines = []
    if (this.state.qso) {
      Object.keys(qso).forEach((key)=> {
        if (key=="APP_QRZLOG_STATUS" && qso[key]=="C"){
          qsl.qrzcom = true
        }
        else if (key=='QSL_RCVD_VIA' && qso[key]=="E") {
          qsl.eqsl = true
        }
        else if (key.startsWith("APP_LoTW_")) {
          eqsl.lotw = true
        }
        else if (key.startsWith("_") || key.endsWith("_")) {
          //internal
        }

        lines.push(<tr key={key}><td>{key}</td><td>{qso[key].toString()}</td></tr>);
      });
    }
    return <div>
              Edit {this.id}<br/>
              <table>
              <thead>
              <tr><td></td><td>Send</td><td>Recv</td></tr>
              </thead>
              <tbody>
              <tr>
                <td>eqsl</td>
                <td></td>
                <td>{qsl.eqsl?<i className="fa fa-check"></i>:""}</td>
              </tr>
              <tr>
                <td>qrz.com</td>
                <td></td>
                <td>{qsl.qrzcom?<i className="fa fa-check"></i>:""}</td>
              </tr>
              <tr>
                <td>LoTW</td>
                <td></td>
                <td>{qsl.lotw?<i className="fa fa-check"></i>:""}</td>
              </tr>
              </tbody>
              </table>
              <br/>

              <table>
              <tbody>
              {lines}
              </tbody>
              </table>
            </div>
  }
}

export {QsoEditor}
