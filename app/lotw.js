const EventEmitter = require('events');

class lotw extends EventEmitter {

  constructor(username, password){
    super();
    this.username = username;
    this.password = password;
    console.log("welcome to lotw", username);
  }


  fetchQsls(){
    console.log("requesting lotw");
    this.emit('status', {"LoTW":"active"})
    var url = "https://lotw.arrl.org/lotwuser/lotwreport.adi?login="+this.username+"&password="+this.password+"&qso_query=1"
    return new Promise((resolve, reject) => {
      fetch(url, {credentials: 'same-origin'})
        .then((response)=>{return response.text();})
        .then((text)=>{
          console.log("fetched lotw");
          this.emit('status', {"LoTW":"idle"})
          resolve(text);
        })
        .catch((err)=>{
          this.emit('status', {"LoTW":"error"})
          console.log("something went wrong",err);
        })
    })
  }
}

export {lotw}
