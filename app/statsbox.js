import React, { Component } from 'react';

class StatList extends Component {
  constructor(props){
    super(props)
    this.state = {expanded:false}
    var total = 0;
    var sortable = [];
    for (var key in props.items)
        sortable.push([key, props.items[key]]);
    sortable.sort((a, b) =>{return this.props.order=="ascending"?a[1] - b[1]:b[1] - a[1];});
    this.items = sortable.map((item,i)=>{total+=item[1]; return(<li key={i}>{item[0]}: {item[1]}</li>)})
    if ((props.total) && (props.total>total))
      this.items.push(<li key="999">Undefined: {props.total-total}</li>);
    if (props.count)
      this.items.push(<div key="998"><span>count:{this.items.length}</span><br/></div>)
  }
  render(){

    return(
    <div>
    <b>{this.props.name}</b><br/>
    <ul>
    {(this.items.length <= 10) || (this.state.expanded)? this.items:this.items.slice(0,10)}
    {this.items.length > 10 ? <a href="#" onClick={(event)=>{this.setState({expanded:!this.state.expanded})}}>{this.state.expanded?"less...":"more..."}</a>:""}
    </ul>
    </div>
  );
  }
}
export default class extends Component {
  constructor(props) {
    super(props);
    this.counter = {};
    this.props.logbook.map((qso)=>{
      for (var field in qso)
        {
          if (!(field in this.counter))
            this.counter[field] = {}
          var val = qso[field]
          if (field=='FREQ')
            val = Math.floor(val)
          if (!(val in this.counter[field]))
            this.counter[field][val] = 1;
          else
            this.counter[field][val]++;
        }
    });
  }
  render(){
    return(
      <div>
      <StatList name="Band" items={this.counter["FREQ"]} total={this.props.logbook.length}/>
      <StatList name="Mode" items={this.counter["MODE"]} />
      <StatList name="Country" items={this.counter["country_"]} count="true"/>
      </div>
    );
  }

}
