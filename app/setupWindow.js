import React, { Component } from 'react';
const electron = window.require("electron")
var fs = require("fs");
var path = require('path')
const userDataPath = (electron.app || electron.remote.app).getPath('userData');

//------------------------------------------------------------------------------
class Setup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {callsign:null}
  }
  handleCallsign(event){
    this.setState({callsign:this.refs.callsign})
    this.setState({isValid:(this.refs.callsign.value.length >= 4)})
  }
  handleCreateCallsign(event){
    fetch("http://ido.dvrdns.org:8080/uberlog/api/v1/register/"+this.refs.callsign.value+"?password="+this.refs.password.value)
    .then((response)=>{return response.json()})
    .then((json)=>{
      console.log(json);
    })
    .catch((err)=>{
       this.setState({error:"can not reach server"})
    })
  }
  handleDownload(event){
    var window = electron.remote.getCurrentWindow();
    fetch("http://ido.dvrdns.org:8080/uberlog/api/v1/callsign/"+this.refs.callsign.value+"?password="+this.refs.password.value)
    .then((response)=>{return response.json()})
    .then((json)=>{
      console.log(json);
      if (json['status'] == 'ok'){
          let result = json['config'].map(({ value }) => value)
		  console.log(result);
          fs.writeFile(path.join(userDataPath, './config.json'), JSON.stringify(result, null, 2) , 'utf8', ()=>{} ); // write it back
		  //window.close();         
        }
      else
        this.setState({error:json.reason})
    })
    .catch((err)=>{
      console.log(err);
       this.setState({error:"can not reach server"})
    })
  }

  handleStandalone(event){
    fetch("http://ido.dvrdns.org:8080/uberlog/api/v1/template/"+this.refs.callsign.value)
    .then((response)=>{return response.json()})
    .then((json)=>{
      console.log(json);
      if (json['status'] == 'ok'){
          //window.close();
          fs.writeFile(path.join(userDataPath, './config.json'), JSON.stringify(json['config'], null, 2) , 'utf8', ()=>{} ); // write it back
        }})
    .catch((err)=>  {
      console.log(err);
        this.config = {
            "callsign" : this.refs.callsign.value,
            "isSpecialCallsign": false,
            "grid" : "",
            "rig" : [],
            "antenna" : [],
            "hamqth" : { "username":"callsign", "password":"password" },
            "eqsl" : { "username":"callsign", "password":"password", "msg":"tnx for qso. 73 GL" },
            "pageSize" : "Letter"
        }
        fs.writeFile(path.join(userDataPath, './config.json'), JSON.stringify(json['config'], null, 2) , 'utf8', ()=>{} ); // write it back
      })
  }

  render(){
    return (
      <div>
      <h1>Setup</h1>
      <p>
      callsign <input ref="callsign" name="callsign" onChange={this.handleCallsign.bind(this)} /><br/>
      password <input ref="password" name="password" type="password" /><br/>
      </p>
      <p style={{"color":"Red"}}>{this.state.error}</p>
      <a href="#" id="confdl" onClick={this.handleDownload.bind(this)}>download configuration from server</a><br/>
      <a href="#" id="confnew" onClick={this.handleCreateCallsign.bind(this)}>setup new server account</a><br/>
      <a href="#" id="confloc" onClick={this.handleStandalone.bind(this)}>continue without sync</a><br/>
      <p id="debug"></p>
      </div>
    )
  }
}


export {Setup}
