import React, { Component } from 'react';

export default class extends Component {
  constructor(props){
    super(props)
  }
  componentDidUpdate(prevProps, prevState, snapshot){
  }
  updateCanvas() {
    var canvas = this.refs.canvas
    var img = this.refs.azmap;

    canvas.style.position = "absolute";
    canvas.style.left = img.offsetLeft + "px";
    canvas.style.top = img.offsetTop + "px";
    canvas.width = img.width
    canvas.height = img.height

    const ctx = this.refs.canvas.getContext('2d');
    ctx.beginPath();
    ctx.strokeStyle="#941100";
    ctx.moveTo(canvas.width/2,0);
    ctx.lineTo(canvas.width/2,canvas.height/2);
    ctx.stroke();
    ctx.beginPath();
    ctx.strokeStyle="#011993";
    ctx.moveTo(canvas.width/2,canvas.height/2);
    ctx.lineTo(canvas.width/2,canvas.height);
    ctx.stroke();
    //this.forceUpdate();
  }

  render(){
    return(
    <div id="mapdiv">
    <input type="number" name="azimuth" value={this.props.az} onChange={this.props.onChange}/>
    <img ref="azmap" src="AzimuthalMap.png"
        style={{'maxWidth': '100%', 'transform':'rotate('+this.props.az+'deg)'}}
        onLoad={this.updateCanvas.bind(this)}
        />
    <canvas ref="canvas" width={300} height={300}/>
    </div>
    )
  }
}
