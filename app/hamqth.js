const EventEmitter = require('events');

class hamqth  extends EventEmitter {
  constructor(username, password){
    super()
    this.appname = "userlog";
    var url = "https://www.hamqth.com/xml.php?u="+username+"&p="+ password;
    fetch(url)
      .then((response) => {
          return response.text();
        })
      .then((text)=>{
        var el = document.createElement( 'html' );
        el.innerHTML = text;
        this.session_id = el.getElementsByTagName( 'session_id' )[0].innerText;
      })
      .catch((err)=>{ console.log(err);})
  }

  lookup(callsign){
    return new Promise((resolve, reject) => {
	  console.log("hamqth:"+callsign);
      if (callsign == "")
        reject(callsign+": no callsign")
      var url = "https://www.hamqth.com/xml.php?id="+this.session_id+"&callsign="+callsign+"&prg"+ this.appname;
      this.emit('status', {"HamQTH":"active"})
      fetch(url)
      .then((response) => {
          return response.text();
        })
      .then((text)=>{
        var el = document.createElement( 'html' );
        el.innerHTML = text;
        var rv ={}
        if (el.getElementsByTagName('error').length == 0){
          rv.callsign = el.getElementsByTagName( 'callsign' )[0].innerText.trim();
          try {
            rv.name = el.getElementsByTagName('nick')[0].innerText.trim();
            rv.qth = el.getElementsByTagName('qth')[0].innerText.trim();
            if (rv.qth.match(/^\d*/))
              rv.qth = rv.qth.substring(rv.qth.indexOf(" "))
            var gridTag = el.getElementsByTagName('grid')[0];
            if (gridTag)
              rv.grid = gridTag.innerText.trim();
          } catch(e) {
            console.log(e);
          }
          this.emit('status', {"HamQTH":"idle"})
          resolve(rv);
        }
        else {
          this.emit('status', {"HamQTH":"error"})
          reject(callsign+": "+el.getElementsByTagName('error')[0].innerText)
        }
      })
    });
  }

}
export {hamqth}
