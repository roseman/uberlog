// ES6 Component
// Import React and ReactDOM
import React from 'react';
import ReactDOM from 'react-dom';
var fs = require("fs");
var path = require('path')
const electron = window.require("electron")
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
const dialog = (electron.dialog || electron.remote.dialog)
const {ipcRenderer} = window.require('electron')
var moment = require('moment');
var blobUtil = require('blob-util')
const url = require('url')

require("font-awesome-webpack");

import {Tab, Tabs} from './ui';
import {InputToolbar, QsoListToolbar} from './toolbar';
import InputBox from './inputbox';
import StatsBox from './statsbox';
import SettingsBox from './settingsBox'
import LogBox from './logbox';
import FooterBox from './footer';
import AzMapBox from './azmap'
import {QsoEditor} from './qsoedit'
import {PSKViewer, PSKReporter} from './pskreporter'
import {ModalTextbox} from './modals'
import {Setup} from './setupWindow'

import helpers from './helpers'
import { logbook } from './logbook'
import dxcc from './dxcc';
import { eqsl } from './eqsl';
import { wsjtx } from './wsjtx'
import { fllog } from './fllog'
import { lotw } from './lotw'
import { qrzcom } from './qrzcom'
import {gridSquareToLatLon} from './HamGridSquare'

import {ElectronRoute, ElectronRouter } from './electronrouter'

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

var BUILD_DIR = path.resolve(__dirname, 'build');
var APP_DIR = path.resolve(__dirname, 'src');
//------------------------------------------------------------------------------
// Search component created as a class
class App extends React.Component {
    constructor(props) {
      super(props);
      var urlParams = new URLSearchParams(window.location.search);
      console.log("urlParams", urlParams)
      console.log("callsign:",urlParams.get('callsign'))
      try {
        var cs = [];
        console.log("loading config",path.join(userDataPath, './config.json'));
        this.all_configs = JSON.parse(fs.readFileSync(path.join(userDataPath, './config.json')));
        console.log("configs");
        console.log(this.all_configs );
        if (this.all_configs.length !== undefined)
        {
          for (var i in this.all_configs) {
            console.log();
            if ((urlParams.get('callsign') == this.all_configs[i].callsign)
             || ((urlParams.get('callsign')=="") && (this.all_configs[i].active)))
            {
              this.config = this.all_configs[i]
            }
           cs.push(this.all_configs[i].callsign);
          }
          this.callsigns = cs;
          if (this.config === undefined)
            this.config = this.all_configs[0]
        }
        else
          this.config = this.all_configs;
      }
      catch (x) {
        console.log("error",x);
        this.handleSetupBox();
        /*
        this.config = {
            "callsign" : "NO0CALL",
            "grid" : "",
            "rig" : [],
            "antenna" : [],
            "hamqth" : { "username":"callsign", "password":"password" },
            "eqsl" : { "username":"callsign", "password":"password", "msg":"tnx for qso. 73 GL" },
            "pageSize" : "Letter"
        }
        fs.writeFile(path.join(userDataPath, './config.json'), JSON.stringify(this.config, null, 2) , 'utf8', ()=>{} ); // write it back
        */
      }

      try {
        this.settings = JSON.parse(fs.readFileSync(path.join(userDataPath, './settings2.json')));
      } catch (e) {
        this.settings = { freq:"14", mode:"SSB", sat:"" }
      }
      this.state = { searchText:null, freq:this.settings.freq, mode:this.settings.mode, logbook:[], isLoaded:false, lastSeen:null, pluginStat:{}, antennaAzimuth:0, sat:this.settings.sat }

      this.logbook = new logbook(this.config.callsign);
      this.logbook.setDxcc (dxcc);
      this.logbook.on('change', (logbook) => {
        document.lastqso = logbook[0]
        this.setState({logbook:logbook, isLoaded:true});
      });
      this.logbook.on('status', (obj) => { this.setState({pluginStat: Object.assign({}, this.state.pluginStat, obj)}) })
      try {
        this.logbook.syncWith('u_'+this.config.server.username, this.config.server.password, "http://ido.dvrdns.org:5984/", "log_"+this.config.callsign.toLowerCase())
      } catch (e) {
          console.log("error",e);
      } finally {

      }
      dxcc.load().then(()=>{this.logbook.updateLogbook()});

      try {
        this.eqsl = new eqsl(this.config.eqsl.username, this.config.eqsl.password);
        this.eqsl.on('status', (obj) => { this.setState({pluginStat: Object.assign({}, this.state.pluginStat, obj)}) })
      } catch (error) {

      }

      try {
        this.lotw = new lotw(this.config.lotw.username, this.config.lotw.password);
        this.lotw.on('status', (obj) => { this.setState({pluginStat: Object.assign({}, this.state.pluginStat, obj)}) })
      } catch (error) {

      }

      try {
        this.qrzcom = new qrzcom(this.config.qrzcom.key)
        this.qrzcom.on('status', (obj) => { this.setState({pluginStat: Object.assign({}, this.state.pluginStat, obj)}) })
      } catch (error) {

      }

      //if (!fs.existsSync(path.join(userDataPath,"QSLs")))
      //  fs.mkdirSync(path.join(userDataPath,"QSLs"));

      this.wsjtx = new wsjtx();
      this.wsjtx.on('qso', this.handleWsjtxQso.bind(this))
      this.wsjtx.on('heartbeat', this.heartbeat.bind(this))
      this.wsjtx.on('status', (obj) => { this.setState({pluginStat: Object.assign({}, this.state.pluginStat, obj)}) })

      this.fllog = new fllog();
      this.fllog.on('status', (obj) => { this.setState({pluginStat: Object.assign({}, this.state.pluginStat, obj)}) })
      this.fllog.on('qso', this.handleFldigiQso.bind(this))
//      this.fllog.on("callsign", (qso)=>{console.log("FLDIGI",qso);this.handleNewCallsign(qso["CALL"])})

      this.pskreporter = new PSKReporter(this.config.callsign);
      this.pskreporter.on('tick', (count) => { this.setState({psktimer:count}) })
      this.pskreporter.on('status', (obj) => { this.setState({pluginStat: Object.assign({}, this.state.pluginStat, obj)}) })
      this.pskreporter.on('reports', (list) => { this.setState({pskreports:list}) })

/*
      fetch('https://api.ipify.org?format=json')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.ip = data.ip
        fetch('https://ipstack.com/ipstack_api.php?ip='+data.ip)
        .then((response) => {
            return response.json();
          })
      .then((data) => {
          this.setState({geodata:data});
        });
      })
*/
      let mycoord = gridSquareToLatLon(this.config["grid"])
      var intervalID = setInterval(()=>{
        this.getTemprature(mycoord).then((temp)=>{
          this.setState({"temprature":temp})
          clearTimeout(intervalID);
          })
      }, 5000);


    }

    componentDidMount(){

    }

//------------------------------------------------------------------------------
//                      Tabs
//------------------------------------------------------------------------------

    handleTabChange(e){
      this.pskreporter.setActive(e.innerText=="Reporter")
    }

//------------------------------------------------------------------------------
//                            Toolbar
//------------------------------------------------------------------------------
    getTemprature(latlon){
      let weatherurl = "http://api.openweathermap.org/data/2.5/weather?lat="+latlon[0]+"&lon="+latlon[1]+"&appid=2b658d7ece0429cc47e94b12666f099d&units=metric"
      return new Promise((resolve, reject)=>{
        fetch(weatherurl)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          resolve(Math.round(data['main']['temp']))
        })
      })
    }

    handleSearch(text){
       this.setState({searchText:text})
     }

    handleToolbarChanged(event){
      var newState = {}
      newState[event.target.id] = event.target.value;
      this.settings[event.target.id] = event.target.value;
      console.log(newState);
	    this.setState(newState);
    }

    handleExportPDF(event){
        var file_path = dialog.showSaveDialog({
          properties: ['openFile'],
          filters: [
            {name: 'PDF', extensions: ['pdf']},
            {name: 'All Files', extensions: ['*']}
          ]
        })
        if (!file_path)
          return
        var print_win = new electron.remote.BrowserWindow({'auto-hide-menu-bar':true});
        var lines = []
        var pagenum = 0
        var theader = `<tr>
                      <td width="10px"></td>
                      <th width="80px">Date</th>
                      <th width="50px">Time</th>
                      <th width="90px">Callsign</th>
                      <th width="60px">Country</th>
                      <th colspan="2">Signal<br/>H | M</th>
                      <th width="40px">Freq</th>
                      <th width="40px">Mode</th>
                      <th width="20px">Name</th>
                      <th width="60px">QTH</th>
                      <th width="20px"></th>
                      <th colspan="2">QSL<br/>S|R</th>
                      <th>Remarks</th>
                      </tr>`
        var style =`<style>
                    #customers {
                        border-collapse: collapse;
                    }

                    #customers td, #customers th {
                        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                        vertical-align: bottom;
                        border: 0px solid #ddd;
                        padding: 2px;

                        overflow: hidden;
                        text-overflow: ellipsis;
                        white-space: nowrap;
                    }

                    #customers tr:nth-child(even){
                      background-color: #f2f2f2;
                    }

                    #customers td.number {background-color: #ffffff;}

                    #customers th {
                        padding-top: 8px;
                        padding-bottom: 8px;
                        text-align: center;
                        background-color: #4CAF50;
                        color: white;
                    }
                    div.footnotes {
                      text-align: right;
                      page-break-after: always;
                    }
                    `
        lines.push('data:text/html;charset=UTF-8,')
        lines.push('<HTML><HEAD>')
        lines.push('<style>'+style+'</style>')
        lines.push('</HEAD><BODY>')
        lines.push('<table id="customers" style="width:100%">')
        lines.push(theader)
        var list = this.state.logbook
        list = list.sort((a,b)=>{return a["_id"]>b["_id"]?1:-1})
        list.map((qso, i)=>{
            var datetime = moment.utc(qso["QSO_DATE"] + " " + qso["TIME_ON"] , "YYYYMMDD HHmm").locale("en-gb")
            var remarks = [];
            if (qso["COMMENT"]) remarks.push(qso["COMMENT"])
            if (qso["MY_RIG"]) remarks.push(qso["MY_RIG"])
            if (qso["MY_ANTENNA"]) remarks.push(qso["MY_ANTENNA"])
            if (qso["MY_CITY"]) remarks.push(qso["MY_CITY"])
            lines.push("<tr>")
            lines.push('<td width="30px" class="number">'+(i+1)+"</td>")
            lines.push("<td>"+datetime.format("L")+"</td>")
            lines.push("<td>"+datetime.format("LT")+"</td>")
            lines.push("<td>"+qso["CALL"]+"</td>")
            lines.push("<td>"+qso["country_"]+"</td>")
            lines.push('<td width="40px">'+(qso["RST_SENT"]?qso["RST_SENT"]:"")+"</td>")
            lines.push('<td width="40px">'+(qso["RST_RCVD"]?qso["RST_RCVD"]:"")+"</td>")
            lines.push("<td>"+Math.floor(qso["FREQ"])+"</td>")
            lines.push("<td>"+qso["MODE"]+"</td>")
            lines.push("<td>"+(qso["NAME"]?qso["NAME"]:"")+"</td>")
            lines.push("<td>"+(qso["QTH"]?qso["QTH"]:"")+"</td>")
            lines.push('<td width="45px">'+(qso["GRID"]?qso["GRID"]:"")+"</td>")
            lines.push('<td width="10px">'+(qso["QSL_SENT"]=="Y"?"&#x2713;":"")+"</td>")
            lines.push('<td width="10px">'+(qso["QSL_RCVD"]=="Y"?"&#x2713;":"")+"</td>")
            lines.push("<td>"+remarks.join(" | ")+"</td>")
            lines.push("</tr>")
            if ((i+1)%25==0){
              pagenum += 1;
              lines.push("</table>")
              lines.push("<br/>")
              lines.push('<div class="footnotes">Page '+pagenum+'</div>')
              lines.push("<br/>")
              lines.push('<table id="customers" style="width:100%">')
              lines.push(theader)
            }

        });
        lines.push("</table>")
        lines.push("</BODY></HTML>")
        print_win.loadURL(lines.join('\n'));
        print_win.show();

          var option = {
            landscape: true,
            marginsType: 2,
            printBackground: true,
            printSelectionOnly: false,
            pageSize: this.config.pageSize, // was 'A4',
          };
        print_win.webContents.on('did-finish-load', () => {
          print_win.webContents.printToPDF(option, (err, data) => {
                 if (err) {
                   dialog.showErrorBox('Error', err);
                   return;
                 }
                 fs.writeFile(file_path, data, (err) => {
                   if (err) {
                     dialog.showErrorBox('Error', err);
                     return;
                   }
                   print_win.close()
                 });
               });
          });
        print_win.on('closed', function() {
          print_win = null;
        });
    }

    handleExportAdif(event){
      var fname = dialog.showSaveDialog({
        properties: ['openFile'],
        filters: [
          {name: 'ADIF', extensions: ['adi', 'adif']},
          {name: 'Text', extensions: ['txt', 'text']},
          {name: 'All Files', extensions: ['*']}
        ]
      })
      if (fname)
        this.logbook.writeLogbookToFile(fname)
    }

    handleImportAdif(event){
      var fname = dialog.showOpenDialog({
        properties: ['openFile'],
        filters: [
          {name: 'ADIF', extensions: ['adif']},
          {name: 'Text', extensions: ['txt', 'text']},
          {name: 'All Files', extensions: ['*']}
        ]
      })
      console.log(fname);
    }

    openSpecialCallsign(event){
      this.setState({showSpecialCallsign:true})
    }

    closeSpecialCallsign(event){
      this.setState({showSpecialCallsign:false})
    }

    handleSpecialCallsign(value){
      console.log(value);
      // we need to add "station_callsign"
      this.config.orig_callsign = this.config.callsign
      this.config.callsign = value
      this.config.isSpecialCallsign = true
      this.setState({showSpecialCallsign:false})
    }

    openGuestOperator(event){
      this.setState({showOperatorName:true})
    }

    closeGuestOperator(event){
      this.setState({showOperatorName:false})
    }

    handleGuestOperator(name){
      this.config.operator_name = name
      this.setState({showOperatorName:false})
    }

    changeLogbook(event){
      console.log(event.target.innerText);
      ipcRenderer.send('change-callsign', event.target.innerText) // prints "pong"
    }
//------------------------------------------------------------------------------
//                            InputBox
//------------------------------------------------------------------------------

    handleNewCallsign(callsign)
    {
      var max = moment.utc(0);
      var rv = null;
      this.state.logbook.map((qso)=>{
        var ts= moment(qso["timestamp_"])
        if ((qso["CALL"]==callsign) && (max<ts))
        {
          rv = qso;
          max = ts;
        }
      });
      this.setState({lastSeen:rv});
    }

    handleSeeMore(event){
	    this.setState({searchText:this.state.lastSeen["CALL"]})
	}

    handleUserSubmit(qso){
      var translate = {"callsign":"CALL", "his":"RST_SENT", "mine":"RST_RCVD", "date":"QSO_DATE", "time":"TIME_ON", "freq":"FREQ"};
      var out = {};
      for (var key in qso)
        out[key in translate ? translate[key]:key.toUpperCase()] = qso[key].trim();
      this.writeQSO(out);
      return true;
    }

    writeQSO(qso){
      if ((qso.CALL =="") || (qso.RST_SENT==""))
        return false;
      if (qso.QSO_DATE=="")
        qso.QSO_DATE = moment().utc().format("YYYYMMDD");
      if (qso.TIME_ON=="")
        qso.TIME_ON = moment().utc().format("HHmm");
      if (!qso.FREQ)
        qso.FREQ = this.state.freq;
      var m = (this.state.mode+"|").split("|",2)
      if (!qso.MODE) {
        qso.MODE = m[0];
        if (m[1]!='')
          qso.SUBMODE = m[1];
      }
      if (this.state.antenna)
        qso["MY_ANTENNA"] =  this.state.antenna;
      if (this.state.rig)
        qso["MY_RIG"] = this.state.rig;
      if (this.state.city)
        qso["MY_CITY"] = this.state.city;
      if (this.config.isSpecialCallsign)
        qso["STATION_CALLSIGN"] = this.config.callsign
      if (this.config.operator_name)
        qso["MY_NAME"] = this.config.operator_name
      if (this.state.sat)
        {
          var FreqLUT = { "V":144, "U":440, "S":2400, "X":10000 }
          var s = this.state.sat.split("|",2)
          qso["PROP_MODE"] = "SAT"
          qso["SAT_NAME"] = s[0]
          qso["FREQ"] = FreqLUT[s[1][0]]
          qso["FREQ_RX"] = FreqLUT[s[1][1]]
        }
      this.logbook.append(qso);
    }
//------------------------------------------------------------------------------
//                          antenna azimuth
//------------------------------------------------------------------------------

    handleManualAzimuth(event){
      this.setState({'antennaAzimuth':event.target.value % 360})
    }

//------------------------------------------------------------------------------
//                               WSJT-X
//------------------------------------------------------------------------------

    heartbeat(msg){
    //      console.log(msg);
    }

    handleWsjtxQso(qso){
      console.log(qso);
      this.writeQSO(qso);
    }

//------------------------------------------------------------------------------
//                           fldigi
//------------------------------------------------------------------------------
    handleFldigiQso(qso){
      console.log(qso);
      this.writeQSO(qso);
    }


//------------------------------------------------------------------------------
//                                  eQSL
//------------------------------------------------------------------------------

    handleShowMap(){
      // see https://developers.google.com/maps/documentation/javascript/markers
      let toLatLon = (coor) => { return {lat:coor[0], lng:coor[1] }}

      const loadView = ({home, list}) => {
        return (`
          <!DOCTYPE html>
          <html>
            <head>
              <style>
                 /* Set the size of the div element that contains the map */
                #map {
                  height: 600px;  /* The height is 400 pixels */
                  width: 100%;  /* The width is the width of the web page */
                 }
              </style>
            </head>
            <body>
              <h3>My Google Maps Demo</h3>
              <!--The div element for the map -->
              <div id="map"></div>
              <script>
          // Initialize and add the map
          function initMap() {
            // The location of Uluru
            var home = ${home};
            var uluru = ${list};
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 4, center: home});
            // The marker, positioned at Uluru
            uluru.forEach((item)=>{
              if (item)
                var marker = new google.maps.Marker({
                  position: item.location,
                  label:item.callsign,
                  icon:"http://maps.google.com/mapfiles/ms/icons/"+item.color+".png",
                  map: map
                });
            })
          }
              </script>
              <!--Load the API from the specified URL
              * The async attribute allows the browser to render the page while the API loads
              * The key parameter will contain your own API key (which is not needed for this tutorial)
              * The callback parameter executes the initMap() function
              -->
              <script async defer
              src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap">
              </script>
            </body>
          </html>
        `)
      }
      var f = this.state.logbook.map((item)=>{
         if (item["GRID"])
         {
           try {
             let location = toLatLon(gridSquareToLatLon(item["GRID"]));
             let color = "red";
             if (item["MODE"] == "SSB")
               color = "green";
             if ((item["MODE"] == "FT8") || (item["MODE"] == "JT65"))
               color = "blue"
             if (item["MODE"] == "PSK")
                 color = "purple";
             return {callsign:item["CALL"], grid:item["GRID"], location:location, color:color}
           } catch(error){
             console.log("bad grid "+item["GRID"]+" for "+item["CALL"]);
           }
         }
      })
      let window = new electron.remote.BrowserWindow({
          width: 528+15, height: 660+32,
          parent: electron.remote.getCurrentWindow(),
          modal: false
        })
      var file = 'data:text/html;charset=UTF-8,' + encodeURIComponent(loadView({
        title: "Map",
        home:JSON.stringify(toLatLon(gridSquareToLatLon(this.config["grid"]))),
        list: JSON.stringify(f)
      }));
      window.loadURL(file);
      // window.webContents.openDevTools()
    }

//------------------------------------------------------------------------------
//                                  eQSL
//------------------------------------------------------------------------------

    bestMatch(qso, list){
      var rv = null;
      var diff = 9999;
      for (var j in list)
        if ((qso.CALL == list[j].CALL) &&
            (qso.QSO_DATE == list[j].QSO_DATE))
            {
              console.log("match", list[j]);
              var a = qso.timestamp_;
              var b = list[j].timestamp_;
              var duration = Math.abs(moment.duration(a.diff(b)).asMinutes());
              qso["diff_"] = duration;
              if ((duration<=15) || (Math.abs(duration-120)<=15) || (Math.abs(duration-180)<=15))
              {
                if (duration<diff){
                  diff = duration;
                  rv = list[j];
                }
              }
           }
       return rv;
    }

    handleQSLRcvdClick(event){
      var target = event.target;
      this.logbook.getById(event.target.id).then((qso)=>{
        qso["QSL_RCVD"] = target.checked?"N":"Y";
        this.logbook.updateQso(qso)
        })

    }

    handleQSLSentClick(event){
      var target = event.target;
      this.logbook.getById(event.target.id).then((qso)=>{
        qso["QSL_SENT"] = target.checked?"N":"Y";
        this.logbook.updateQso(qso)
        })
    }

    handleShowEQsl(event){
      var id = event.target.id
      const loadView = ({title,imageBase64}) => {
        return (`
          <!DOCTYPE html>
          <html>
            <head>
              <title>${title}</title>
              <meta charset="UTF-8">
            </head>
            <body>
            <img alt="Embedded Image"
                src="data:image/png;base64,${imageBase64}" />
            </body>
          </html>
        `)
      }
      this.logbook.getAttachment(event.target.id, 'eqsl')
      .then((blob) => {
        blobUtil.blobToBase64String(blob).then( (base64String) =>{
          let window = new electron.remote.BrowserWindow({
              width: 528+15, height: 336+32,
              parent: electron.remote.getCurrentWindow(),
              modal: false
            })
          var file = 'data:text/html;charset=UTF-8,' + encodeURIComponent(loadView({
            title: "eQSL "+id,
            imageBase64: base64String
          }));
          window.loadURL(file);
        }).catch( (err) => {
          console.log(err);
        })
      })
    }

    handleSendEQSL(event){
      this.logbook.getById(event.target.id).then((origqso)=>{
        var qso = origqso;
        if (qso["MODE"].startsWith("PSK")) {
          qso["SUBMODE"] = qso["MODE"];
          qso["MODE"] = "PSK"
        }
        qso['QSLMSG'] = this.config.eqsl.msg;
        // send to eqsl
        this.eqsl.sendEQsl(helpers.objectToAdif(qso))
        .then(()=>{
          origqso["QSL_SENT"] = "Y";
          this.logbook.updateQso(origqso)
          })
        .catch((error)=>{})
        //send to qrz.com
        qso["STATION_CALLSIGN"] = this.config.callsign;
        this.qrzcom.sendQsl(helpers.objectToAdif(qso))
        .then(()=>{
          origqso["QSL_SENT"] = "Y";
          this.logbook.updateQso(origqso)
        })
      })
    }

    handleEqslSync(event){
      var count = 0
        this.wsjtx.loadlog();
        this.eqsl.fetchQsls()
        .then((text)=>{
          console.log("merging eqsl");
          var list = this.state.logbook;
          var qsls = helpers.parseAdifFile(text);
          for (var i in qsls){
            this.logbook.calculateQsoFields(qsls[i]);
            var bestMatch = this.bestMatch(qsls[i], list);
            if (bestMatch !== null){
              var needUpdate = false
              // copy qsl fields
              for (var field in qsls[i])
              {
                var f = field.replaceAll("SENT","RCVD")
                // only update fields relevant to QSLs
                if ((field.includes("QSL")) && (bestMatch[f] != qsls[i][field])) {
                  bestMatch[f] = qsls[i][field];
                  needUpdate = true
                }
              }
              if (needUpdate) {
                 count++
                 this.logbook.updateQso(bestMatch);
               }

              // download picture
              if (('_attachments' in bestMatch) && ('eqsl' in bestMatch['_attachments'])){
                // nothing to do
              }
              else {
                this.eqsl.fetchImageAlt(bestMatch["_id"], qsls[i])
                  .then((res)=>{
                    this.eqsl.archive(qsls[i]);
                    this.logbook.attachImageFromUrl(res.id,"eqsl",res.url);
                    })
                  .catch((x)=>{console.log(x);})
              }

            }
            else {
              console.log("no match ",qsls[i]);
              if (qsls[i].MODE == "FT8")
                this.wsjtx.findMatching(qsls[i]);
            }
          }

        });

        if (count)
          alert("got {0} new eqsls".format(count))
    }

//------------------------------------------------------------------------------
//                              EDIT
//------------------------------------------------------------------------------

    handleEditBox (event){
      const startUrl = path.resolve('index.html');
      var id = event.target.id
      let window = new electron.remote.BrowserWindow({
          width: 528+15, height: 336+32,
          parent: electron.remote.getCurrentWindow(),
          modal: false,
          title:"QSO Edit"
        })
      window.loadURL(url.format({
        pathname: startUrl,
        protocol: 'file:',
        slashes: true,
        query: {qsoedit: id, iam:this.config.callsign}
      }))
//      if (isDev)
        window.webContents.openDevTools()
      window.on('closed', function () {
          window = null
        })
    }

    handleSetupBox(event){
      console.log('launching setup box');
      const startUrl = path.resolve('index.html');
      let window = new electron.remote.BrowserWindow({
          width: 528+15, height: 336+32,
          parent: electron.remote.getCurrentWindow(),
          modal: false,
          title:"Setup"
        })
      window.loadURL(url.format({
        pathname: startUrl,
        protocol: 'file:',
        slashes: true,
        query: {setup: true}
      }))
//      if (isDev)
        window.webContents.openDevTools()
      window.on('closed', function () {
          window = null
        })
    }

    handleSetupUpload(event){
      console.log("upload");
      var url = "http://ido.dvrdns.org:8080/uberlog/api/v1/callsign/"+this.config.server.username+"?password="+this.config.server.password
      fetch(url, {
          method: 'post',
          body: JSON.stringify(this.all_configs)
      })
      .then((data) => {return data.json();})
      .then((json)=> {console.log(JSON.stringify(json))})
      .catch((error) => {console.error(error)});
    }

    handleSetupSave(event){

    }

    handleSettings(event){
      console.log('launching settings box');
      const startUrl = path.resolve('index.html');
      let window = new electron.remote.BrowserWindow({
          width: 528+15, height: 336+32,
          parent: electron.remote.getCurrentWindow(),
          modal: false,
          title:"Settings"
        })
      window.loadURL(url.format({
        pathname: startUrl,
        protocol: 'file:',
        slashes: true,
        query: {settings: true}
      }))
//      if (isDev)
        window.webContents.openDevTools()
      window.on('closed', function () {
          window = null
        })
    }
//------------------------------------------------------------------------------
//                         LoTW
//------------------------------------------------------------------------------

    handleLotwSync(event){
      var count = 0
      this.lotw.fetchQsls()
      .then((text)=>{
        console.log("merging lotw");
        var list = this.state.logbook;
        var qsls = helpers.parseAdifFile(text);
        qsls.forEach((qsl)=>{
          this.logbook.calculateQsoFields(qsl);
          var bestMatch = null;
          for (var i in list){
            if (list[i].CALL.trim() == qsl.CALL.trim() ){
              bestMatch = list[i]
            }
          }
          if (bestMatch != null){
            var needUpdate = false
            // copy qsl fields
            for (var field in qsl)
            {
              var f = field.replaceAll("SENT","RCVD")
              var v = typeof qsl[field] == "string" ? qsl[field].trim() : qsl[field]
              // only update fields relevant to QSLs
              if ((field.includes("QSL") || field.includes("LoTW")) && ((bestMatch[f] === undefined) || (bestMatch[f] != v) )) {
                bestMatch[f] = v;
                needUpdate = true
              }
            }
            if (needUpdate){
               count++
               console.log("count",count);
               this.logbook.updateQso(bestMatch);
             }
          }
        })
        if (count>0)
          alert("got "+count+" new qsls")
      })
    }

//------------------------------------------------------------------------------
// QRZ.com logbook
//------------------------------------------------------------------------------
handleQRZComSync(){
  this.qrzcom.fetchQsls()
  .then((text)=>{
    console.log("merging qrzcom");
    var list = this.state.logbook;
    var qsls = helpers.parseAdifFile(text, false);
    var count = 0
    qsls.forEach((qsl)=>{
      console.log(qsl);
      this.logbook.calculateQsoFields(qsl);
      var bestMatch = this.bestMatch(qsl, list);
      if (bestMatch !== null){
        var needUpdate = false
        // copy qsl fields
        for (var field in qsl)
        {
          var f = field.replaceAll("SENT","RCVD")
          // only update fields relevant to QSLs
          if ((field.includes("QRZLOG")) && (bestMatch[f] != qsl[field])) {
            bestMatch[f] = qsl[field];
            needUpdate = true
          }
        }
        if (needUpdate) {
           console.log("updated");
           count++
           this.logbook.updateQso(bestMatch);
         }
       }
       else {
         console.log("no match");
       }
    })
    if (count)
      alert("got "+count+" new eqsls")
    console.log('done');
  })
}

//------------------------------------------------------------------------------
// render method is most important
// render method returns JSX template
//------------------------------------------------------------------------------
    render() {
        fs.writeFile(path.join(userDataPath, './settings2.json'), JSON.stringify(this.settings), (err)=>{});
        return (
          <div id="grid">
            <div id="toolbarLeftArea" className="toolbar-button">
               <InputToolbar freq = {this.state.freq}
                        mode={this.state.mode}
                        rigs={this.config.rig}
                        rig={this.state.rig}
                        antennas={this.config.antenna}
                        antenna={this.state.antenna}
                        sat={this.state.sat}

                        onFreqChanged={this.handleToolbarChanged.bind(this)}
                        onModeChanged={this.handleToolbarChanged.bind(this)}
                        onSatChanged={this.handleToolbarChanged.bind(this)}
                        onSomethingChanged={this.handleToolbarChanged.bind(this)}
                        />
            </div>
            <div id="toolbarRightArea" className="toolbar-button">
               <QsoListToolbar
                        callsign={this.config.callsign}
                        callsigns={this.callsigns}
                        grid={this.config.grid}
                        temprature={this.state.temprature}
                        onSearch={this.handleSearch.bind(this)}
                        onEqslSync={this.handleEqslSync.bind(this)}
                        onLoTWSync={this.handleLotwSync.bind(this)}
                        onQRZComSync={this.handleQRZComSync.bind(this)}
                        onExportAdif={this.handleExportAdif.bind(this)}
                        onExportPDF={this.handleExportPDF.bind(this)}
                        onImportAdif={this.handleImportAdif.bind(this)}
                        onSpecialCallsign={this.openSpecialCallsign.bind(this)}
                        onGuestOperator={this.openGuestOperator.bind(this)}
                        onChangeLogbook={this.changeLogbook.bind(this)}
                        onSettings={this.handleSettings.bind(this)}
                        onMap={this.handleShowMap.bind(this)}
                        />
            </div>
            <div id="inputArea">
               <Tabs onClick={this.handleTabChange.bind(this)}>
                 <Tab name="Input">
                   <InputBox onSubmit={this.handleUserSubmit.bind(this)}
                             onNewCallsign={this.handleNewCallsign.bind(this)}
						                 onSeeMore={this.handleSeeMore.bind(this)}
						                 lastSeen={this.state.lastSeen}
                           />
                 </Tab>
                 <Tab name="Stats">
                   <StatsBox logbook={this.state.logbook}/>
                 </Tab>
                 {/*
                 <Tab name="Settings">
                   <SettingsBox settings={this.config}
                                onSave={this.handleSetupSave.bind(this)}
                                onUpload={this.handleSetupUpload.bind(this)}
                   />
                 </Tab>
                 */}
                 <Tab name="Reporter">
                   <PSKViewer reports={this.state.pskreports} countdown={this.state.psktimer}/>
                 </Tab>
                 <Tab name="Map">
                   <AzMapBox az={this.state.antennaAzimuth} onChange={this.handleManualAzimuth.bind(this)}/>
                 </Tab>
                 </Tabs>
            </div>
            <div id="logArea">
              <LogBox logbook={this.state.logbook}
                      isLoaded={this.state.isLoaded}
                      filterText={this.state.searchText}
                      onRcvdClick = {this.handleQSLRcvdClick.bind(this)}
                      onSentClick = {this.handleQSLSentClick.bind(this)}
                      onSendEQsl = {this.handleSendEQSL.bind(this)}
                      onShowEQsl = {this.handleShowEQsl.bind(this)}
                      onEditBox = {this.handleEditBox.bind(this)}
                      />
            </div>
            <div id="footerArea">
               <FooterBox count={this.state.logbook.length} stat={this.state.pluginStat}/>
            </div>

            <ModalTextbox show={this.state.showSpecialCallsign}
                          text="Special callsign"
                          value={this.config.callsign}
                          onClose={this.closeSpecialCallsign.bind(this)}
                          onSelect={this.handleSpecialCallsign.bind(this)}/>
            <ModalTextbox show={this.state.showOperatorName}
                          text="Operator name"
                          value={this.config.operator_name}
                          onClose={this.closeGuestOperator.bind(this)}
                          onSelect={this.handleGuestOperator.bind(this)}/>
          </div>
        );
    }
}


// Render to ID content in the DOM
ReactDOM.render( <ElectronRouter>
                    <ElectronRoute path="callsign" component={<App/>} default />
                    <ElectronRoute path="qsoedit" component={<QsoEditor/>} />
                    <ElectronRoute path="setup" component={<Setup/>} />
                    <ElectronRoute path="settings" component={<SettingsBox/>} />
                 </ElectronRouter>,
    document.getElementById('content')
);
