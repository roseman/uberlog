const url = require('url')
const path = require('path')
const { Before, After, Given, When, Then } = require('cucumber')

Given(/^I launch the application$/, function (callback) {
    this.signInWindow = this.createWindow({width: 800, height: 600});
    //this.signInWindow.loadURL('file://' + __dirname + '/signin.html');
    let callsign = ""
    this.signInWindow.loadURL(url.format({
      pathname: path.join(__dirname,'..','..', 'index.html'),
      protocol: 'file:',
      slashes: true,
      query: {setup: true}
    }))
    this.signInWindow.webContents.openDevTools()
    callback();
   });

When('I enter a/my {word} username and password into the form', function (usertype, callback) {
         this.typeText(this.signInWindow, 'callsign', 't3mp')
         .then(() => this.typeText(this.signInWindow, 'password', 'password'))
         .then(() => this.clickButton(this.signInWindow, 'confdl'))
         .then(() => this.done('done', callback))
         .catch((err)=>callback(err))
    });

When(/^I ask to setup (.*) account$/, function (setuptype, callback) {
           // Write code here that turns the phrase above into concrete actions
           callback(null, 'pending')
         });

Then('I will be provided with a/my {word} setup file', function (setuptype, callback) {
           // Write code here that turns the phrase above into concrete actions
           this.signInWindow.close()
           callback(null, 'pending')
         });

Then(/^I will be given access to the application$/, function (callback) {
    // callback(null, 'pending');
    return setTimeout(function(){
        console.log("timeout")
        callback();
      }, 1000);
    });
