const url = require('url')
const path = require('path')
const { Before, After, Given, When, Then } = require('cucumber')
const assert = require('assert');
const fetch = require("node-fetch");

Given(/^I choose ([A-Z]+-[0-9]+) option on satellite list$/, function (sat_name, callback ) {
           // Write code here that turns the phrase above into concrete actions
           this.sat = sat_name
           callback();
         });

When('I log a QSO', function (callback) {
           this.typeText(this.mainWindow, 'Text1', "t3mp\n59\n59\n\n\n")
           .then(() => this.clickButton(this.mainWindow, 'process'))

           var text = '';
           this.typeText(this.mainWindow, 'Text1', "hello")
           .then(() => {
             this.readText(this.mainWindow, 'Text1', text)
             .then((result)=>console.log("text", result))
             .then(()=>this.done("dome", callback))
             .catch((error)=>this.done(error, callback))
           });

           // return setTimeout(function(){
           //     callback();
           //   }, 5000);

         });

Then(/^log record field (.*) should be (.*)$/, function (key, value) {
           // Write code here that turns the phrase above into concrete actions

           console.log("key",key, "value", value);
           assert.equal(value, this.sat)
         });
