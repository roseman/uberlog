// features/support/world.js
const { setWorldConstructor, setDefaultTimeout, Before } = require('cucumber')
var electron = require("electron")
var app = require('electron').app
const fetch = require("node-fetch");
const { ipcMain } = require('electron')

class World {
  constructor(){
  }

  createWindow (options) {
    const { BrowserWindow } = require('electron');
    return new BrowserWindow(options);
  };

  typeText(window, id, text) {
    text = text.replace(/\n/g, "\\n"); // fix multiline textarea
    return new Promise(function(resolve ,reject) {
      var code = `var input = document.querySelector("[name='${id}']");
        if (!input) {
          'FAIL: An input element with identifier ${id} was not found';
        } else {
          input.value = '${text}';
        }`
      window.webContents.executeJavaScript(code, true, function(result) {
        succeedOrFail(result, resolve, reject);
      });
    });
  };

  clickButton(window, id) {
    return new Promise(function(resolve, reject) {
      var code = `var button = document.querySelector("[id='${id}']");
        if (!button) {
          'FAIL: A button with identifier ${id} was not found';
        } else {
          button.click();
          'OK'
        }`
      window.webContents.executeJavaScript(code, true, function(result) {
        succeedOrFail(result, resolve, reject);
      });
    });
  };

  readText(window, id) {
    return new Promise(function(resolve ,reject) {
      var code = `var input = document.querySelector("[name='${id}']");
        if (!input) {
          'FAIL: An element with identifier ${id} was not found';
        } else {
          '${input.value}'
        }`
      window.webContents.executeJavaScript(code, true, function(result) {
        succeedOrFail(result, resolve, reject);
      });
    });
  };

  done(msg, cb) {
    console.log("done",msg);
    cb();
  }

  get_logbook(){
    ipcMain
  }
}


var succeedOrFail = function (result, resolve, reject) {
 if (result.startsWith('FAIL:')) {
   reject(new Error(result.substring(5).trim()));
 } else {
   resolve(result);
 }
}


setWorldConstructor(World)
