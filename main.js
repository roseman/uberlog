const electron = require('electron')
const {Menu} = require('electron')
const {ipcMain} = require('electron')
var fs = require("fs");
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')

const getFromEnv = parseInt(process.env.ELECTRON_IS_DEV, 10) === 1;
const isEnvSet = 'ELECTRON_IS_DEV' in process.env;

const isDev = isEnvSet ? getFromEnv : (process.defaultApp || /node_modules[\\/]electron[\\/]/.test(process.execPath));

if (isDev){
  require('electron-reload')(__dirname);
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

const template = [
    {
        label: 'Electron',
        submenu: [
        {
            label: 'Options',
            click: function() {
                alert('Test');
            }
        }
        ]
    }
]

function createWindow (callsign) {
  // Create the browser window.
  let userDataPath = app.getPath('userData');
  let settings
  try {
    settings = JSON.parse(fs.readFileSync(path.join(userDataPath, './settings1.json')));
  } catch (e) {
    settings = {width:1275, height:600};
    fs.writeFileSync(path.join(userDataPath, './settings1.json'), JSON.stringify(settings));
  }
  mainWindow = new BrowserWindow({width: settings.width, height: settings.height, title:"UBerLog",webPreferences: {
    experimentalFeatures: true,
  }})

  // and load the index.html of the app.
  console.log(__dirname);
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true,
    query: {callsign: callsign}
  }))

  // Open the DevTools.
  if (isDev)
    mainWindow.webContents.openDevTools()

  //const menu = Menu.buildFromTemplate(template)
  //Menu.setApplicationMenu(menu)
  mainWindow.on('resize',()=>{
    var size   = mainWindow.getSize();
    var width  = size[0];
    var height = size[1];

    let settings = {width:size[0], height:size[1]};
    fs.writeFileSync(path.join(userDataPath, './settings1.json'), JSON.stringify(settings));
  })

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('change-callsign', (event, arg) => {
    mainWindow.close()
    console.log("switching to", arg);
    createWindow(arg)
  })

ipcMain.on('always-on-top', (event, arg) => {
  mainWindow.setAlwaysOnTop(arg, "floating", 1);
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
